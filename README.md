# Codementor Test Assessment

This repo contains my solution for a coding challenge, as a part of CodementorX's hiring process

# Getting Started

clone repo by running:

```
git clone https://github.com/MeghdadHadidi/ideapool-mhadidi.git
```

then run following command to in order to install dependencies

```
npm install
```

Now, for starting the project in dev mode run following command

```
npm start
```

... and, following command to get a production build

```
npm run start:prod
```

## Challenge

A simple React app to fetch data from CodementorX's platform

## Solution

-   As the time was limited, so I decided to use my own React/Webpack boilerplate project that I configured before
-   I used `Redux` as a state management tool, of course to get the extra point :-)
-   I used `Fractal` approach to struct the project, as it normally gives a more handy way to maintain the project as it goes larger and larger
-   A single webpack.config file is used to manage both `dev` and `prod` environments, I just wanted to keep most of the settings by webpack's default
-   I decided to use extra loaders to have a better css result: postcss, postcss-rtl (to have auto-generated RTL styles)
-   To get safely access to deeply nested properties of objects -- from api result -- I had a choice of using libraries (like Ramda, ...) but I decided to write my own solution as a util
-   PropTypes is used to control `Home` component's props
-   I used stateless components for products list and single products
