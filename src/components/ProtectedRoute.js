import React from "react"
import { Route, Redirect } from "react-router-dom"
import { connect } from "react-redux"

const ProtectedRoute = ({ component: Component, ...rest }) => {
	const { authenticated } = rest
	return (
		<Route
			{...rest}
			render={props => {
				if (authenticated) {
					return <Component {...props} />
				}

				return <Redirect to="/signup" />
			}}
		/>
	)
}

const mapStateToProps = ({ auth }) => ({
	authenticated: auth.authenticated
})

export default connect(
	mapStateToProps,
	{}
)(ProtectedRoute)
