import React from "react"

const Avatar = ({ user }) => {
	return (
		<div className="avatar-holder">
			<img src={user.avatar_url} alt={user.email} />
		</div>
	)
}

export default Avatar
