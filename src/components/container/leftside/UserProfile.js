import React, { Component } from "react"
import PropTypes from "prop-types"
import { connect } from "react-redux"

import Avatar from "./userprofile/Avatar"
import Logout from "components/Logout"

import { getCurrentUser } from "modules/auth/user"

class UserProfile extends Component {
	componentDidMount() {
		if (this.props.authenticated && this.props.token) {
			this.props.getCurrentUser()
		}
	}

	render() {
		const { user } = this.props
		return (
			<div className="user-container">
				{user.email && (
					<>
						<Avatar user={user} />
						<span>{user.name}</span>
						<Logout />
					</>
				)}
			</div>
		)
	}
}

UserProfile.defaultProps = {
	user: {}
}

UserProfile.propTypes = {
	loading: PropTypes.bool,
	user: PropTypes.object.isRequired,
	authenticated: PropTypes.bool.isRequired
}

const mapStateToProps = ({ user, auth }) => {
	return {
		loading: user.fetching,
		user: user.detail || {},
		authenticated: auth.authenticated,
		token: auth.token
	}
}

export default connect(
	mapStateToProps,
	{ getCurrentUser }
)(UserProfile)
