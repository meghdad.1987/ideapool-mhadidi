import React from "react"
import { Link } from "react-router-dom"

import smallLogo from "images/IdeaPool_icon.png"
import largeLogo from "images/IdeaPool_icon@2x.png"

class Brand extends React.Component {
	render() {
		return (
			<div className="brand-container">
				<img
					alt="Idea Pool"
					src={smallLogo}
					srcSet={`${smallLogo} 300w, ${largeLogo} 1980w`}
				/>
				<Link to="/">Got Ideas?</Link>
			</div>
		)
	}
}

export default Brand
