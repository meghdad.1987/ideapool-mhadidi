import React from "react"

import Brand from "./leftside/Brand"
import UserProfile from "./leftside/UserProfile"

const LeftSide = () => {
	return (
		<div className="left-side">
			<Brand />
			<UserProfile />
		</div>
	)
}

export default LeftSide
