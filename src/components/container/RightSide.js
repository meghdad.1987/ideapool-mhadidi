import React from "react"

const RightSide = ({ children }) => {
	return (
		<div className="right-side">
			<div className="container">{children}</div>
		</div>
	)
}

export default RightSide
