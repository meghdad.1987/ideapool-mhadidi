import React from "react"

const Alert = ({ alerts }) => {
	return (
		<div className="alerts-container">
			{alerts &&
				alerts.map((alert, key) => (
					<div key={key} className={`alert ${alert.type}`}>
						{alert.message}
					</div>
				))}
		</div>
	)
}

export default Alert
