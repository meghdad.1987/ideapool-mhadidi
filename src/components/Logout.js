import React, { Component } from "react"
import { connect } from "react-redux"

import { sendLogoutRequest } from "modules/auth/user"

import "components/Alert"

class Logout extends Component {
	handleLogout = () => {
		this.props.sendLogoutRequest()
	}

	render() {
		return (
			<div className="logout-link">
				<a onClick={this.handleLogout}>Log Out</a>
			</div>
		)
	}
}

const mapStateToProps = () => ({})

export default connect(
	mapStateToProps,
	{ sendLogoutRequest }
)(Logout)
