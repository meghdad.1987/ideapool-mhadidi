import React, { Component } from "react"
import PropTypes from "prop-types"

import LeftSide from "./container/LeftSide"
import RightSide from "./container/RightSide"

class Container extends Component {
	render() {
		return (
			<div className="wrapper">
				<LeftSide />
				<RightSide children={this.props.children} />
			</div>
		)
	}
}

Container.propTypes = {
	children: PropTypes.element
}

export default Container
