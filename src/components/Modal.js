import React from "react"

const Modal = ({
	title,
	content,
	approveText,
	cancelText,
	approveCallback,
	cancelCallback
}) => {
	function handleApproveClick() {
		approveCallback && approveCallback()
	}

	function handleCancelClick() {
		cancelCallback && cancelCallback()
	}

	function handleKeyPress(e) {
		if (e.which == 27) {
			handleCancelClick()
		} else if (e.which == 13) {
			handleApproveClick()
		}
	}

	return (
		<div onKeyPressCapture={handleKeyPress} className="modal-container">
			<div className="modal">
				<div className="modal-title">
					<h2>{title}</h2>
				</div>
				<div className="modal-body">
					<p>{content}</p>
				</div>
				<div className="modal-actions">
					<button className="cancel" onClick={handleCancelClick}>
						{cancelText || `Cancel`}
					</button>
					<button className="approve" onClick={handleApproveClick}>
						{approveText || `OK`}
					</button>
				</div>
			</div>
		</div>
	)
}

export default Modal
