import api from "utils/api"

import { setNewToken } from "modules/auth"

export const refreshMiddleware = ({ dispatch, getState }) => next => action => {
	if (typeof action != "function") {
		return next(action)
	}

	const { token, refToken, expiresAt } = getState().auth
	const threasholdFromNow = new Date().getTime() + 550000

	if (token && threasholdFromNow > Number(expiresAt)) {
		api.post("/access-tokens/refresh", { refresh_token: refToken }).then(
			data => {
				dispatch(setNewToken({ token: data.jwt }))
				action.token = data.jwt
				return next(action)
			}
		)
	} else {
		action.token = token
		return next(action)
	}
}
