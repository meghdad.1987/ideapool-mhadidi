import { sortWith, path, prop, descend, ascend } from "ramda"

export const sortByAverageDesc = sortWith([descend(prop("average_score"))])

export const getPayloadType = res => path(["payload", "type"], res)
