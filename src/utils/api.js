import axios from "axios"
import { apiBaseUrl, protectedEndpoints } from "config"

const instance = axios.create({
	baseURL: apiBaseUrl,
	timeout: 60000,
	headers: {
		"Content-Type": "application/json"
	}
})
axios.defaults.headers["Content-Type"] = "application/json"

const request = (method, url, data) => {
	return new Promise((resolve, reject) => {
		;(() => {
			let accessToken = localStorage.getItem("token")
			let isProtected = protectedEndpoints.indexOf(url) != -1
			console.log("Token: ", accessToken)
			console.log("URL: ", url)
			console.log("Is Protected: ", isProtected)

			if (accessToken && isProtected) {
				axios.defaults.headers["X-Access-Token"] = accessToken
			}
			if (method === "get") {
				return instance.request({
					url,
					method,
					params: data
				})
			} else {
				return instance.request({
					url,
					mode: "cors",
					method,
					data
				})
			}
		})()
			.then(res => {
				resolve(res.data)
			})
			.catch(err => {
				reject(err.response)
			})
	})
}

export default {
	get: (endpoint, data) => {
		return request("get", endpoint, data)
	},
	post: (endpoint, data) => {
		return request("post", endpoint, data)
	},
	put: (endpoint, data) => {
		return request("put", endpoint, data)
	},
	del: (endpoint, data) => {
		return request("delete", endpoint, data)
	}
}
