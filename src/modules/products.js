import api from 'utils/api';

export default function reducer(state={}, action) {
  switch (action.type) {
    case 'apps.fetchingProductsList': 
      return {...state, fetchingProductsList: true};

    case 'apps.fetchSuccess':
      return {...state, fetchingProductsList: false, ...action.result};

    default: 
      return state;
  }
}

// actions
export function getProductsList() {
  return (dispatch) => {
    dispatch({type: 'apps.fetchingProductsList'});
    api
      .get(`/serier/samtliga`)
      .then(result => {
        dispatch({type: 'apps.fetchSuccess', result});
      })
      .catch(err => {
        console.log(err);
        dispatch({type: 'apps.fetchError', err});
      })
    ;
  };
}