import api from "utils/api"
import { tokenTimeout } from "../config"

let token = localStorage.getItem("token")
let refToken = localStorage.getItem("refToken")
let expiresAt = localStorage.getItem("expiresAt")

const initialState = token
	? {
			authenticated: true,
			token: token,
			refToken: refToken,
			expiresAt
	  }
	: { authenticated: false }

export default function reducer(state = initialState, action) {
	switch (action.type) {
		case "auth.fetchingToken":
			return {
				...state,
				refreshing: true
			}
		case "auth.tokenFetched":
			return {
				...state,
				refreshing: false,
				token: action.payload,
				expiresAt: new Date().getTime() + tokenTimeout
			}
		case "auth.loginSuccess":
			return {
				...state,
				authenticated: true,
				refreshing: false,
				token: action.payload.jwt,
				refToken: action.payload.refresh_token,
				expiresAt: new Date().getTime() + tokenTimeout
			}
		case "auth.error":
			return {
				...state,
				refreshing: false,
				token: null,
				refToken: null,
				error: action.payload
			}
		case "auth.delete":
			return {
				...state,
				authenticated: false,
				token: null,
				refToken: null,
				expiresAt: 0
			}
		default:
			return state
	}
}

export function setNewToken({ token, refresh_token }) {
	return dispatch => {
		localStorage.setItem("expiresAt", new Date().getTime() + 600000)
		localStorage.setItem("token", token)
		if (refresh_token) {
			localStorage.setItem("refToken", refresh_token)
		}

		dispatch({ type: "auth.tokenFetched", payload: token })
	}
}

export function refreshToken(refresh_token = initialState.refToken) {
	return dispatch => {
		return api
			.post("/access-tokens/refresh", { refresh_token })
			.then(({ jwt }) => {
				setNewToken(jwt)
			})
			.catch(res => {
				var error = ""
				if (res.data && res.data.reason) {
					error = res.data.reason
				}
				if (typeof res === "string") {
					error = res
				}

				return dispatch({
					type: "auth.error",
					payload: {
						type: "error",
						message: error
					}
				})
			})
	}
}
