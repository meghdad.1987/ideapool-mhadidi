import api from "utils/api"
import { tokenTimeout } from "../../config"
import { setNewToken } from "../auth"

export default function reducer(state = {}, action) {
	switch (action.type) {
		case "auth.signup.start":
			return {
				...state,
				signingUp: true
			}
		case "auth.signup.end":
			return {
				...state,
				signingUp: false
			}
		case "auth.signup.error":
			return {
				...state,
				signingUp: false,
				error: action.payload
			}
		default:
			return state
	}
}

export function sendSignupRequest({ email, name, password }) {
	return dispatch => {
		dispatch({ type: "auth.signup.start" })

		return api
			.post("/users", { email, name, password })
			.then(res => {
				dispatch({ type: "auth.signup.end" })
				dispatch({ type: "auth.loginSuccess", payload: res })
				dispatch(
					setNewToken({
						token: res.jwt,
						refresh_token: res.refresh_token
					})
				)

				return res
			})
			.catch(res => {
				var error = ""
				if (res.data && res.data.reason) {
					error = res.data.reason
				}
				if (typeof res === "string") {
					error = res
				}

				return dispatch({
					type: "auth.signup.error",
					payload: {
						type: "error",
						message: error
					}
				})
			})
	}
}
