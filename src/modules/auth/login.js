import api from "utils/api";
import { setNewToken } from "../auth";

export default function reducer(state = {}, action) {
  switch (action.type) {
    case "auth.login.start":
      return {
        ...state,
        loggingIn: true,
        error: null
      };
    case "auth.login.end":
      return {
        ...state,
        loggingIn: false
      };
    case "auth.login.error":
      return {
        ...state,
        loggingIn: false,
        error: action.payload
      };
    default:
      return state;
  }
}

export function sendLoginRequest({ email, password }) {
  return dispatch => {
    dispatch({ type: "auth.login.start" });

    return api
      .post("/access-tokens", { email, password })
      .then(res => {
        dispatch({ type: "auth.login.end" });
        dispatch({ type: "auth.loginSuccess", payload: res });

        dispatch(
          setNewToken({
            token: res.jwt,
            refresh_token: res.refresh_token
          })
        );

        return res;
      })
      .catch(res => {
        var error = "";
        if (res.data && res.data.reason) {
          error = res.data.reason;
        }
        if (typeof res === "string") {
          error = res;
        }

        return dispatch({
          type: "auth.login.error",
          payload: {
            type: "error",
            message: error
          }
        });
      });
  };
}
