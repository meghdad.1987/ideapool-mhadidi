import api from "utils/api"
import history from "../../customHistory"

var userInStorage = localStorage.getItem("user")
var expiresAt = localStorage.getItem("expiresAt")
var threasholdFromNow = new Date().getTime() + 100000
var expired = threasholdFromNow > Number(expiresAt)
var initState =
	userInStorage && !expired ? { detail: JSON.parse(userInStorage) } : {}

export default function reducer(state = initState, action) {
	switch (action.type) {
		case "auth.user.fetching":
		case "auth.user.logout.start":
			return {
				...state,
				fetching: true
			}
		case "auth.user.success":
			return {
				...state,
				fetching: false,
				detail: action.payload
			}
		case "auth.user.logout.end":
			return {
				...state,
				detail: null
			}
		case "auth.user.error":
			return {
				...state,
				fetching: false,
				detail: null,
				error: action.payload
			}
		default:
			return state
	}
}

export function getCurrentUser() {
	return dispatch => {
		dispatch({ type: "auth.user.fetching" })

		api.get("/me")
			.then(res => {
				if (res.email) {
					localStorage.setItem("user", JSON.stringify(res))
					dispatch({ type: "auth.user.success", payload: res })
				}

				return res
			})
			.catch(res => {
				var error = ""
				if (res.data && res.data.reason) {
					error = res.data.reason
				}
				if (typeof res === "string") {
					error = res
				}

				return dispatch({
					type: "auth.user.error",
					payload: {
						type: "error",
						message: error
					}
				})
			})
	}
}

export function sendLogoutRequest() {
	return (dispatch, getState) => {
		dispatch({ type: "auth.user.logout.start" })

		return api
			.del("/access-tokens", { refresh_token: getState().auth.refToken })
			.then(res => {
				localStorage.removeItem("token")
				localStorage.removeItem("refToken")
				localStorage.removeItem("user")
				dispatch({ type: "auth.user.logout.end" })
				dispatch({ type: "auth.delete" })
				history.push("/signup")

				return res
			})
			.catch(res => {
				var error = ""
				if (res.data && res.data.reason) {
					error = res.data.reason
				}
				if (typeof res === "string") {
					error = res
				}

				return dispatch({
					type: "auth.user.error",
					payload: {
						type: "error",
						message: error
					}
				})
			})
	}
}
