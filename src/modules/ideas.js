import api from "utils/api"
import IdeaList from "../pages/home/IdeaList"

const initialState = {
	items: []
}

export default function reducer(state = initialState, action) {
	switch (action.type) {
		case "ideas.fetching":
			return {
				...state,
				fetching: true
			}
		case "ideas.fetched":
			return {
				...state,
				fetching: false,
				items: action.payload
			}
		case "ideas.new": {
			let items

			if (action.payload) {
				items = state.items.map(item => {
					if (!item.id) {
						item = action.payload
					}
					return item
				})
			} else {
				items = [
					{
						id: "",
						content: "",
						impact: 10,
						ease: 10,
						average_score: 10,
						confidence: 10
					},
					...state.items
				]
			}
			return {
				...state,
				items
			}
		}
		case "ideas.save": {
			const items = state.items.map(item => {
				if (item.id == action.payload.id) {
					item = action.payload
				}
				return item
			})
			return {
				...state,
				items
			}
		}
		case "ideas.remove":
			return {
				...state,
				items: state.items.filter(item => item.id != action.payload)
			}
		case "ideas.error":
			return {
				...state,
				fetching: false,
				error: action.payload
			}
		case "ideas.delete":
			return {
				...state
			}
		default:
			return state
	}
}

export function createIdea({ content, impact, ease, confidence }) {
	return dispatch => {
		return api
			.post("/ideas", { content, impact, ease, confidence })
			.then(data => {
				dispatch({ type: "ideas.new", payload: data })
			})
			.catch(res => {
				var error = ""
				if (res.data && res.data.reason) {
					error = res.data.reason
				}
				if (typeof res === "string") {
					error = res
				}

				return dispatch({
					type: "ideas.error",
					payload: {
						type: "error",
						message: error
					}
				})
			})
	}
}

export function deleteIdea(id) {
	return dispatch => {
		return api
			.del(`/ideas/${id}`)
			.then(data => {
				dispatch({ type: "ideas.remove", payload: id })
			})
			.catch(res => {
				var error = ""
				if (res.data && res.data.reason) {
					error = res.data.reason
				}
				if (typeof res === "string") {
					error = res
				}

				return dispatch({
					type: "ideas.error",
					payload: {
						type: "error",
						message: error
					}
				})
			})
	}
}

export function updateIdea({ id, content, impact, ease, confidence }) {
	return dispatch => {
		return api
			.put(`/ideas/${id}`, { content, impact, ease, confidence })
			.then(data => {
				dispatch({ type: "ideas.save", payload: data })
			})
			.catch(res => {
				var error = ""
				if (res.data && res.data.reason) {
					error = res.data.reason
				}
				if (typeof res === "string") {
					error = res
				}

				return dispatch({
					type: "ideas.error",
					payload: {
						type: "error",
						message: error
					}
				})
			})
	}
}

export function getIdeas(p) {
	return dispatch => {
		return api
			.get("/ideas")
			.then(data => {
				dispatch({ type: "ideas.fetched", payload: data })
			})
			.catch(res => {
				var error = ""
				if (res.data && res.data.reason) {
					error = res.data.reason
				}
				if (typeof res === "string") {
					error = res
				}

				return dispatch({
					type: "ideas.error",
					payload: {
						type: "error",
						message: error
					}
				})
			})
	}
}
