import React from "react"
import ReactDOM from "react-dom"
import { Provider } from "react-redux"
import { Router, Route, Switch } from "react-router-dom"
import customHistory from "./customHistory"

import ProtectedRoute from "components/ProtectedRoute"
import ErrorBoundry from "components/ErrorBoundry"
import Login from "pages/Login"
import Signup from "pages/Signup"
import NotFound from "components/NotFound"

import "index.scss"
import registerServiceWorker from "utils/registerServiceWorker"
import store from "store"

import Home from "pages/Home"

const App = () => (
	<Provider store={store}>
		<Router history={customHistory}>
			<Switch>
				<ProtectedRoute exact path="/" component={Home} />
				<Route exact path="/login" component={Login} />
				<Route exact path="/signup" component={Signup} />
				<Route component={NotFound} />
			</Switch>
		</Router>
	</Provider>
)

ReactDOM.render(<App />, document.getElementById("root"))
registerServiceWorker()
