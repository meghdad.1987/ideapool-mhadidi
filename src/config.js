const apiBaseUrl = "https://small-project-api.herokuapp.com"
const tokenTimeout = 600000
const protectedEndpoints = ["/me", "/ideas"]

export { apiBaseUrl, tokenTimeout, protectedEndpoints }
