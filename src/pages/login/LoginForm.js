import React, { Component } from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { getPayloadType } from "utils/ideas";
import Alert from "components/Alert";

import { sendLoginRequest } from "modules/auth/login";

class LoginForm extends Component {
  state = {
    form: {
      email: "",
      password: ""
    }
  };

  handleChange = e => {
    this.setState({
      ...this.state,
      form: {
        ...this.state.form,
        [e.target.name]: e.target.value
      }
    });
  };

  handleSubmit = e => {
    e.preventDefault();
    this.props.sendLoginRequest(this.state.form).then(res => {
      if (getPayloadType(res) != "error") {
        this.props.onSuccess();
      }
    });
  };

  render() {
    const {
      form: { email, password }
    } = this.state;
    const { loading, error } = this.props;
    return (
      <form method="post" onSubmit={this.handleSubmit}>
        {error && <Alert alerts={[error]} />}
        <div className="form-group">
          <input
            placeholder="Email"
            id="email"
            type="text"
            onChange={this.handleChange}
            name="email"
            value={email}
          />
        </div>

        <div className="form-group">
          <input
            id="password"
            placeholder="Password"
            type="password"
            onChange={this.handleChange}
            name="password"
            value={password}
          />
        </div>
        <div className="form-group">
          <button type="submit" disabled={loading}>{loading ? "Logging In ..." : "Login"}</button>
          <div className="direction-link">
            Don't have an account? <Link to="/signup">Sign Up</Link>
          </div>
        </div>
      </form>
    );
  }
}

LoginForm.defaultProps = {
  authenticated: false
};

LoginForm.propTypes = {
  loading: PropTypes.bool,
  authenticated: PropTypes.bool.isRequired
};

const mapStateToProps = ({ login, auth }) => ({
  loading: login.loggingIn,
  authenticated: auth.authenticated,
  error: login.error
});

export default connect(
  mapStateToProps,
  { sendLoginRequest }
)(LoginForm);
