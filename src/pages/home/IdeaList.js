import React from "react";
import { sortByAverageDesc } from "../../utils/ideas";

import Idea from "pages/home/idealist/Idea";

const IdeaList = ({ ideas, onSaveItem, onCancelItem, onDeleteItem }) => {
  let sortedIdeas = sortByAverageDesc(ideas);
  return (
    <div className="idea-list">
      <ul>
        {sortedIdeas &&
          sortedIdeas.length &&
          sortedIdeas.map((idea, key) => (
            <Idea
              onSaveItem={onSaveItem}
              onCancelItem={onCancelItem}
              onDeleteItem={onDeleteItem}
              key={idea.id}
              index={key}
              idea={idea}
            />
          ))}
      </ul>
    </div>
  );
};

export default IdeaList;
