import React from "react"
import store from "../../../store"

const TopToolbar = ({ onAddItem, addFormVisible }) => {
	return (
		<div className="top-toolbar">
			<h1>My Ideas</h1>
			<div
				onClick={onAddItem}
				className={`add-idea-button ${addFormVisible ? "cancel" : ""}`}>
				<span>
					<i>+</i>
				</span>
			</div>
		</div>
	)
}

export default TopToolbar
