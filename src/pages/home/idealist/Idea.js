import React, { Component } from "react"
import PropTypes from "prop-types"

import Modal from "components/Modal"
import Alert from "components/Alert"

import editIconSmall from "images/pen.png"
import editIconLarge from "images/pen@2x.png"
import removeIconSmall from "images/bin.png"
import removeIconLarge from "images/bin@2x.png"

class Idea extends Component {
	state = {
		id: this.props.idea.id,
		content: this.props.idea.content,
		impact: this.props.idea.impact,
		ease: this.props.idea.ease,
		confidence: this.props.idea.confidence,
		average: this.props.idea.average_score,
		editMode: !this.props.idea.id,
		confirmModalVisible: false,
		loading: false,
		formErrors: []
	}

	componentDidMount() {
		this.refs.content.focus()
	}

	static getDerivedStateFromProps(newProps, oldState) {
		const { idea } = newProps
		if (idea.id != oldState.id) {
			return {
				...idea,
				loading: false
			}
		}
		return null
	}

	setLoadingActive = () => {
		this.setState({
			loading: true
		})
	}

	changeEditMode = isActive => {
		if (!isActive) {
			this.refs.content.blur()
		}

		this.setState({
			editMode: isActive
		})
	}

	toggleConfirmModal = () => {
		this.setState({
			confirmModalVisible: !this.state.confirmModalVisible
		})
	}

	validateFieldValues = () => {
		const { content, ease } = this.state
		let formErrors = this.state.formErrors.filter(item => item.id)

		if (!ease || ease < 1) {
			formErrors.push({
				message: `Ease: Ease can not be less than 1`
			})
		}
		if (!content) {
			formErrors.push({
				message: `Content: Idea content can not be empty`
			})
		}

		this.setState({
			formErrors
		})

		return formErrors
	}

	handleSaveClick = e => {
		e.preventDefault()
		const errors = this.validateFieldValues()
		if (errors.length) return
		this.setLoadingActive()
		this.changeEditMode(false)
		this.props.onSaveItem(this.state)
	}

	handleCancelClick = () => {
		this.setLoadingActive()
		this.changeEditMode(false)
		this.props.onCancelItem(this.state.id)
	}

	handleChange = e => {
		this.setState({
			[e.target.name]: e.target.value
		})
	}

	handleModalApprove = () => {
		this.setLoadingActive()
		this.toggleConfirmModal()
		this.props.onDeleteItem(this.state.id)
	}

	handleModalCancel = () => {
		this.toggleConfirmModal()
	}

	handleKeyUp = e => e.which == 27 && this.handleCancelClick()

	render() {
		const { index } = this.props
		const {
			editMode,
			id,
			content,
			ease,
			impact,
			confidence,
			average,
			loading,
			formErrors,
			confirmModalVisible
		} = this.state
		const firstItem = index == 0

		return (
			<>
				{formErrors.length > 0 && <Alert alerts={formErrors} />}
				{confirmModalVisible && (
					<Modal
						title="Are you sure?"
						content="The idea will be permanently deleted"
						cancelCallback={this.handleModalCancel}
						approveCallback={this.handleModalApprove}
					/>
				)}
				<li
					className={`idea-item item-${index} ${
						editMode ? "editable" : "readonly"
					}`}>
					<form
						onKeyUpCapture={this.handleKeyUp}
						onSubmit={this.handleSaveClick}>
						<div
							className={`loading-ball ${
								loading ? "animated" : ""
							}`}>
							<div />
						</div>
						<input type="hidden" name="id" value={id} />
						<div className="form-group idea-content">
							<input
								ref="content"
								type="text"
								name="content"
								id="content"
								required
								value={content}
								onChange={this.handleChange}
							/>
						</div>
						<div className="form-group">
							{firstItem && <label>Impact</label>}
							<div className="dropdown">
								<select
									name="impact"
									value={impact}
									onChange={this.handleChange}>
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
									<option value="6">6</option>
									<option value="7">7</option>
									<option value="8">8</option>
									<option value="9">9</option>
									<option value="10">10</option>
								</select>
							</div>
						</div>

						<div className="form-group">
							{firstItem && <label>Ease</label>}
							<div className="dropdown">
								<select
									name="ease"
									value={ease}
									onChange={this.handleChange}>
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
									<option value="6">6</option>
									<option value="7">7</option>
									<option value="8">8</option>
									<option value="9">9</option>
									<option value="10">10</option>
								</select>
							</div>
						</div>

						<div className="form-group">
							{firstItem && <label>Confidence</label>}
							<div className="dropdown">
								<select
									name="confidence"
									value={confidence}
									onChange={this.handleChange}>
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
									<option value="6">6</option>
									<option value="7">7</option>
									<option value="8">8</option>
									<option value="9">9</option>
									<option value="10">10</option>
								</select>
							</div>
						</div>

						<div className="form-group average">
							{firstItem && <label>Avg.</label>}
							<input
								readOnly
								name="average"
								value={average}
								id="average"
								type="text"
							/>
						</div>
						<div
							className={`form-group action-buttons ${
								!editMode ? "show-on-hover" : ""
							}`}>
							{!editMode ? (
								<>
									<span
										className="edit-idea"
										onClick={() =>
											this.changeEditMode(true)
										}>
										<img
											alt="Edit Idea"
											src={editIconSmall}
											srcSet={`${editIconSmall} 300w, ${editIconLarge} 1980w`}
										/>
									</span>
									<span
										onClick={this.toggleConfirmModal}
										className="remove-idea">
										<img
											alt="Remove Idea"
											src={removeIconSmall}
											srcSet={`${removeIconSmall} 300w, ${removeIconLarge} 1980w`}
										/>
									</span>
								</>
							) : (
								<>
									<span
										onClick={this.handleSaveClick}
										className="save-idea"
									/>
									<span
										className="cancel-idea"
										onClick={this.handleCancelClick}
									/>
								</>
							)}
						</div>
						<input type="submit" hidden />
					</form>
				</li>
			</>
		)
	}
}

Idea.propTypes = {
	onDeleteItem: PropTypes.func.isRequired,
	onSaveItem: PropTypes.func.isRequired,
	onCancelItem: PropTypes.func.isRequired,
	idea: PropTypes.object.isRequired
}

export default Idea
