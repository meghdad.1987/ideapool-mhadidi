import React from "react"

import smallLampIcon from "images/bulb.png"
import largeLampIcon from "images/bulb@2x.png"

const CenterLogo = () => {
	return (
		<div className="centered-logo">
			<img
				alt="Idea Pool"
				src={smallLampIcon}
				srcSet={`${smallLampIcon} 300w, ${largeLampIcon} 1980w`}
			/>
			<p>Got Ideas?</p>
		</div>
	)
}

export default CenterLogo
