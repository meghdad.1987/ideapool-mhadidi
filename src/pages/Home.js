import React, { Component } from "react"
import { connect } from "react-redux"
import PropTypes from "prop-types"
import { withRouter } from "react-router-dom"

// import { getIdeas } from "utils/ideas"
import { getIdeas, createIdea, updateIdea, deleteIdea } from "modules/ideas"

import Container from "components/Container"
import CenterLogo from "pages/home/CenterLogo"
import TopToolbar from "pages/home/idealist/TopToolbar"
import IdeaList from "pages/home/IdeaList"
import Modal from "components/Modal"

class Home extends Component {
	state = {
		fetching: false,
		addFormVisible: false,
		confirmModalVisible: false
	}

	componentDidMount() {
		if (this.props.authenticated) {
			this.props.getIdeas()
		}
	}

	toggleConfirmModal = () => {
		this.setState({
			confirmModalVisible: !this.state.confirmModalVisible
		})
	}

	handleAddItemClick = () => {
		if (this.state.addFormVisible) {
			this.props.removeIdea(0)
		} else {
			this.props.addNewIdea()
		}

		this.setState({
			...this.state,
			addFormVisible: !this.state.addFormVisible
		})
	}

	handleCancelItemClick = id => {
		this.setState({
			...this.state,
			addFormVisible: false
		})

		if (!id) {
			this.props.removeIdea(id)
		}
	}

	handleSaveItemClick = idea => {
		this.setState({
			...this.state,
			addFormVisible: false
		})

		if (idea.id) {
			this.props.updateIdea(idea)
		} else {
			this.props.createIdea(idea)
		}
	}

	handleDeleteItemClick = id => {
		if (id) {
			this.props.deleteIdea(id)
		}
	}

	render() {
		const { ideas, fetching } = this.props
		const { addFormVisible } = this.state
		return (
			<Container>
				<div className="homepage-container">
					<TopToolbar
						onAddItem={this.handleAddItemClick}
						addFormVisible={addFormVisible}
					/>
					{ideas.length != 0 ? (
						<IdeaList
							onSaveItem={this.handleSaveItemClick}
							onDeleteItem={this.handleDeleteItemClick}
							onCancelItem={this.handleCancelItemClick}
							ideas={ideas}
						/>
					) : (
						<CenterLogo />
					)}
				</div>
			</Container>
		)
	}
}

Home.propTypes = {
	fetching: PropTypes.bool,
	ideas: PropTypes.array,
	addNewIdea: PropTypes.func.isRequired,
	removeIdea: PropTypes.func.isRequired,
	getIdeas: PropTypes.func.isRequired,
	createIdea: PropTypes.func.isRequired,
	updateIdea: PropTypes.func.isRequired,
	deleteIdea: PropTypes.func.isRequired
}

Home.defaultProps = {
	fetching: false,
	ideas: []
}

const mapStateToProps = ({ auth, ideas }) => {
	return {
		ideas: ideas.items,
		fetching: ideas.fetching,
		authenticated: auth.authenticated
	}
}

const mapDispatchToProps = dispatch => {
	return {
		addNewIdea: () => dispatch({ type: "ideas.new" }),
		removeIdea: id => dispatch({ type: "ideas.remove", payload: id }),
		getIdeas: () => dispatch(getIdeas()),
		createIdea: idea => dispatch(createIdea(idea)),
		updateIdea: idea => dispatch(updateIdea(idea)),
		deleteIdea: id => dispatch(deleteIdea(id))
	}
}

export default withRouter(
	connect(
		mapStateToProps,
		mapDispatchToProps
	)(Home)
)
