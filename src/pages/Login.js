import React, { Component } from "react"

import Container from "components/Container"
import LoginForm from "./login/LoginForm"

class Login extends Component {
	redirectToHome = () => this.props.history.push("/")

	render() {
		return (
			<Container>
				<div className="login-page form-container">
					<h2>Login</h2>
					<LoginForm onSuccess={this.redirectToHome} />
				</div>
			</Container>
		)
	}
}

export default Login
