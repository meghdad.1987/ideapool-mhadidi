import React, { Component } from "react"
import PropTypes from "prop-types"
import { connect } from "react-redux"
import { Link } from "react-router-dom"
import Alert from "components/Alert"

import { sendSignupRequest } from "modules/auth/signup"

import ErrorBoundry from "../../components/ErrorBoundry"

class SignupForm extends Component {
	state = {
		form: {
			email: "",
			name: "",
			password: ""
		}
	}

	handleChange = e => {
		this.setState({
			...this.state,
			form: {
				...this.state.form,
				[e.target.name]: e.target.value
			}
		})
	}

	handleSubmit = e => {
		e.preventDefault()
		this.props.sendSignupRequest(this.state.form).then(() => {
			this.props.onSuccess()
		})
	}

	render() {
		const {
			form: { email, name, password }
		} = this.state
		const { loading, error } = this.props
		return (
			<form method="post" onSubmit={this.handleSubmit}>
				{error && <Alert alerts={[error]} />}
				<div className="form-group">
					<input
						placeholder="Email"
						id="email"
						type="text"
						onChange={this.handleChange}
						name="email"
						value={email}
					/>
				</div>

				<div className="form-group">
					<input
						id="name"
						placeholder="Full Name"
						type="text"
						onChange={this.handleChange}
						name="name"
						value={name}
					/>
				</div>
				<div className="form-group">
					<input
						id="password"
						placeholder="Password"
						type="password"
						onChange={this.handleChange}
						name="password"
						value={password}
					/>
				</div>
				<div className="form-group">
					<button
						type="submit"
						{...loading && { disabled: "disabled" }}>
						{loading ? "Singing up..." : "Sign Up"}
					</button>
					<ErrorBoundry>
						<div className="direction-link">
							Already have an account?{" "}
							<Link to="/login">Login</Link>
						</div>
					</ErrorBoundry>
				</div>
			</form>
		)
	}
}

SignupForm.defaultProps = {
	authenticated: false
}

SignupForm.propTypes = {
	loading: PropTypes.bool,
	sendSignupRequest: PropTypes.func.isRequired,
	authenticated: PropTypes.bool.isRequired
}

const mapStateToProps = ({ signup, auth }) => ({
	loading: signup.signingUp,
	error: signup.error,
	authenticated: auth.authenticated
})

export default connect(
	mapStateToProps,
	{ sendSignupRequest }
)(SignupForm)
