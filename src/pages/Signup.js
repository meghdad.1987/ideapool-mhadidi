import React, { Component } from "react"

import Container from "components/Container"
import SignupForm from "./signup/SignupForm"

class Signup extends Component {
	redirectToHome = () => this.props.history.push("/")

	render() {
		return (
			<Container>
				<div className="signup-page form-container">
					<h2>Sign Up</h2>
					<SignupForm onSuccess={this.redirectToHome} />
				</div>
			</Container>
		)
	}
}

export default Signup
