import { createStore, applyMiddleware, compose } from "redux"
import logger from "redux-logger"
import thunk from "redux-thunk"
import { combineReducers } from "redux"
import { refreshMiddleware } from "utils/refreshMiddleware"

import auth from "modules/auth"
import login from "modules/auth/login"
import signup from "modules/auth/signup"
import ideas from "modules/ideas"
import user from "modules/auth/user"
import products from "modules/products"

const rootReducer = combineReducers({
	auth,
	user,
	login,
	ideas,
	signup,
	products
})

const store = createStore(
	rootReducer,
	compose(
		applyMiddleware(logger, refreshMiddleware, thunk),
		window.__REDUX_DEVTOOLS_EXTENSION__
			? window.__REDUX_DEVTOOLS_EXTENSION__()
			: f => f
	)
)

export default store
