/*******************************
 * Environment and Imports
 ******************************/
var env = process.env.NODE_ENV.trim()
var devMode = env != "production"

const webpack = require("webpack")
const MiniCssExtractPlugin = require("mini-css-extract-plugin")
const HtmlWebpackPlugin = require("html-webpack-plugin")
const UglifyJsPlugin = require("uglifyjs-webpack-plugin")
const StyleLintPlugin = require("stylelint-webpack-plugin")
const CopyWebpackPlugin = require("copy-webpack-plugin")
const path = require("path")

const here = p => path.resolve(__dirname, p)

/*******************************
 * Entry
 ******************************/
const entry = {
	main: ["./src/index.js"]
}

/*******************************
 * Output
 ******************************/
const output = {
	path: here("build"),
	filename: "[name].bundle.js",
	publicPath: "/"
}

/*******************************
 * Module
 ******************************/
const modules = {
	rules: [
		{
			test: /\.(gif|png|jpe?g|svg)$/i,
			use: [
				"file-loader",
				{
					loader: "image-webpack-loader",
					options: {
						bypassOnDebug: true, // webpack@1.x
						disable: true // webpack@2.x and newer
					}
				}
			]
		},
		{
			test: /\.js$/,
			exclude: /node_modules/,
			use: {
				loader: "babel-loader",
				options: {
					presets: ["@babel/preset-env", "@babel/preset-react"]
				}
			}
		},
		{
			test: /\.(sa|sc|c)ss$/,
			exclude: [/node_modules/],
			use: [
				devMode ? "style-loader" : MiniCssExtractPlugin.loader,
				"css-loader",
				{
					loader: "postcss-loader",
					options: {
						config: {
							path: here("./")
						}
					}
				},
				"sass-loader"
			]
		}
	]
}

/*******************************
 * Plugins
 ******************************/
const plugins = [
	new CopyWebpackPlugin([
		{ from: "./public/favicon.ico", to: "./" },
		{ from: "./public/_redirects", to: "./" },
		{ from: "./public/manifest.json", to: "./" }
	]),
	new MiniCssExtractPlugin({
		filename: devMode ? "[name].css" : "[name].[hash].css",
		chunkFilename: devMode ? "[id].css" : "[id].[hash].css"
	}),
	new HtmlWebpackPlugin({
		template: here("./public/index.html")
	}),
	new StyleLintPlugin({
		configFile: here("./stylelint.config.js")
	})
]

// Enabling HMR only if dev mode is enabled
if (devMode) {
	plugins.push(new webpack.HotModuleReplacementPlugin())
}

/*******************************
 * Optimization
 ******************************/
const optimization = {
	minimizer: [
		new UglifyJsPlugin({
			sourceMap: true,
			parallel: 4,
			cache: true,
			extractComments: true,
			uglifyOptions: {
				comments: /@preserve/i
			}
		})
	]
}

/*******************************
 * Exporting configuration
 ******************************/
var configObject = {
	mode: env,
	entry,
	devServer: {
		historyApiFallback: true,
		port: 4000,
		hot: true
	},
	optimization,
	devtool: "source-map",
	output,
	resolveLoader: {
		// Configure how Webpack finds `loader` modules.
		modules: ["./node_modules"]
	},
	module: modules,
	plugins,
	resolve: {
		alias: {
			utils: here("src/utils"),
			images: here("src/images"),
			components: here("src/components"),
			modules: here("src/modules"),
			pages: here("src/pages")
		},
		modules: [here("src"), "node_modules"]
	}
}

module.exports = (env, argv) => {
	configObject.mode = argv.mode
	return configObject
}
