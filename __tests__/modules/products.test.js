import configureMockStore from "redux-mock-store"
import thunk from "redux-thunk"
import fetchMock from "fetch-mock"

import * as actions from "../../src/modules/ideas"

const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)

describe("async action", () => {
	afterEach(() => {
		fetchMock.restore()
	})

	it("should change fetching state to true", () => {
		fetchMock.getOnce("/ideas", {
			body: [
				{
					id: "ir9td2tvq",
					content: "the-content",
					impact: 3,
					ease: 8,
					confidence: 8,
					average_score: 6.333333333333333,
					created_at: 1524210786
				},
				{
					id: "ir9td2p51",
					content: "the-content",
					impact: 2,
					ease: 8,
					confidence: 8,
					average_score: 6.0,
					created_at: 1524210786
				},
				{
					id: "ir9td2lz8",
					content: "the-content",
					impact: 1,
					ease: 8,
					confidence: 8,
					average_score: 5.666666666666667,
					created_at: 1524210786
				}
			]
		})

		const expectedActions = [
			{ type: "app.fetchingIdeasList" },
			{
				type: "app.fetchSuccess",
				body: [
					{
						id: "ir9td2tvq",
						content: "the-content",
						impact: 3,
						ease: 8,
						confidence: 8,
						average_score: 6.333333333333333,
						created_at: 1524210786
					},
					{
						id: "ir9td2p51",
						content: "the-content",
						impact: 2,
						ease: 8,
						confidence: 8,
						average_score: 6.0,
						created_at: 1524210786
					},
					{
						id: "ir9td2lz8",
						content: "the-content",
						impact: 1,
						ease: 8,
						confidence: 8,
						average_score: 5.666666666666667,
						created_at: 1524210786
					}
				]
			}
		]

		const store = mockStore({})

		return store.dispatch(actions.getIdeas()).then(() => {
			expect(store.getActions()).toEqual(expectedActions)
		})
	})
})
